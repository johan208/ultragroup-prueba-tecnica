if (typeof window !== 'undefined') {
  require('js-loading-overlay');
  JsLoadingOverlay.setOptions({
    overlayBackgroundColor: '#fff',
    overlayOpacity: 0.7,
    spinnerIcon: 'ball-grid-beat',
    spinnerColor: '#cf4646',
    spinnerSize: '2x',
    lockScroll: true,
  });

  var showLoader = () => {
    JsLoadingOverlay.show();
  };

  var hideLoader = () => {
    JsLoadingOverlay.hide();
  };
}
export { showLoader, hideLoader };
