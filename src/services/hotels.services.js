import axios from 'axios';

const clienteAxios = axios.create({
  baseURL: 'https://hotels4.p.rapidapi.com',
  headers: {
    'Access-Control-Allow-Origin' : '*',
    'content-type': 'application/json',
    'X-RapidAPI-Key' : process.env.rapidApiKey,
    "X-RapidAPI-Host" : process.env.rapidApiHost
  }
});

export const getAllHotelsCall = ( body ) => {
    return clienteAxios.post(`/properties/v2/list`, body);
  };

