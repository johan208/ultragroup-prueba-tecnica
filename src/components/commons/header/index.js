import React from "react";
import Link from "next/link";
//icons
import { MdLanguage } from 'react-icons/md';

const Header = () => {
    return ( 
        <nav className="top-0 fixed z-50 w-full px-2 py-3 navbar-expand-lg bg-white shadow">
            <ul className="flex flex-wrap items-center justify-end" >
                <li className="flex items-center text-red-600 text-sm font-bold leading-relaxed mr-4 py-2">
                    <MdLanguage className="mr-1"/>
                    <Link className="" href="#" >
                        Español
                    </Link>
                </li>
                <li className="text-red-600 text-sm font-bold leading-relaxed inline-block mr-4 py-2">
                    <Link href="#" >
                        Iniciar Sesión
                    </Link>
                </li>
            </ul>
        </nav>
     );
}
 
export default Header;