import React, { memo } from 'react';

const ValidationError = ({ formik, name }) => {
  // console.log('renderizado error =====>>>>>')
  if (formik.touched[name] && formik.errors[name]) {
    return (
      <small className="font-semibold leading-normal mt-2 mb-2 text-red-500">
        {formik.errors[name]}
      </small>
    );
  }
  return null;
};

export default memo(ValidationError);
