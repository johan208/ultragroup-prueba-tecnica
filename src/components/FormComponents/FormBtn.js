import React from 'react';

const FormBtn = ({ type, children }) => {
  return (
    <button
      className={
        'relative bg-red-500 hover:bg-red-600  text-white active:bg-red-600 font-bold uppercase text-sm px-6 py-2 mt-1 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-4 mb-1 ease-linear transition-all duration-150'
      }
      type={type ? type : 'submit'}
    >
      {children}
    </button>
  );
};

export default FormBtn;
