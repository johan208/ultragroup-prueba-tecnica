import React, { useState, useEffect } from 'react';

const ControlContainer = ({ hidden, width, children }) => {
  const [anchura, setAnchura] = useState('sm:w-6/12 lg:w-3/12 xl:w-3/12 w-6/12');
  useEffect(() => {
    if (width === 'half') {
      setAnchura('sm:w-6/12 xl:w-4/12  w-6/12');
    }
    if (width === '2/3') {
      setAnchura('sm:w-6/12 xl:w-8/12  w-6/12');
    }
    if (width === 'full') {
      setAnchura('w-full');
    }
  }, [width]);

  return (
    <div
      className={
        hidden
          ? 'hidden'
          : null +
            ` justify-self-center items-center flex flex-col justify-start px-4 mb-4 ${anchura}`
      }
    >
      {' '}
      {children}{' '}
    </div>
  );
};

export default ControlContainer;
