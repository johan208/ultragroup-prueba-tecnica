import React from 'react';

const Label = ({ children }) => {
  // console.log('renderizado label ====>>>>>>>>')
  return (
    <label className="relative self-start block uppercase text-black text-sm font-bold mb-2 ">
      {children}
    </label>
  );
};

export default Label;
