import React from 'react';

const Input = ({ formik, type, name, placeholder }) => {

  return (
    <input
      type={type}
      placeholder={placeholder ? placeholder : ''}
      className="px-3 py-3 placeholder-[#6B7280] text-[#6B7280] relative bg-white rounded text-base border text-sm shadow outline-none focus:shadow-md w-full"
      name={name}
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
      value={formik.values[name]}
    />
  );
};

export default Input;
