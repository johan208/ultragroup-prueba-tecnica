import React from 'react';
// Components
import ValidationError from './ValidationError';
import ControlContainer from './ControlContainer';
import Input from './Input';
import Label from './Label';

const FormBlock = ({
  blockConfig,
  formik
}) => {

  if (blockConfig && blockConfig.field == 'input') {
    return (
      <ControlContainer
        hidden={blockConfig.hidden ? 'hidden' : null}
        width={blockConfig.fieldWidth && blockConfig.fieldWidth}
      >
        <Label> {blockConfig.label} </Label>
        <Input
          formik={formik}
          type={
            blockConfig.type === 'date'
              ? 'date'
              : blockConfig.type === 'number'
              ? 'number'
              : blockConfig.type === 'time'
              ? 'time'
              : 'text'
          }
          placeholder={blockConfig.placeholder ? blockConfig.placeholder : null}
          name={blockConfig.name}
          isReadOnly={blockConfig.isReadOnly && blockConfig.isReadOnly}
        />
        <ValidationError formik={formik} name={blockConfig.name} />
      </ControlContainer>
    );
  } 
};

export default FormBlock;
