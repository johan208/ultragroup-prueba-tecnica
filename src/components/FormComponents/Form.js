import React from 'react'
import { useFormik } from 'formik';
import * as Yup from 'yup';
import useFilterForm from '../../hooks/useFilterForm'

// Components
import FormBlock from './FormBlock';
import FormBtn from './FormBtn';

const Form = ({
    justify
}) => {
    const { initialValues, validationSchema, handleSubmit, inputs } = useFilterForm(  )

    const formFormik = useFormik({
        enableReinitialize: true,
        initialValues: initialValues,
        validationSchema: Yup.object(validationSchema),
        onSubmit: handleSubmit,
      });

    return ( 
    <form
      onSubmit={ formFormik.handleSubmit }
      className={'bg-transparent shadow-lg relative rounded w-3/4 mb-12 p-6' }
    >

      <div className={(justify ? justify : 'justify-between') + ' flex flex-wrap'}>
        {
          inputs.map((blockConfig, index) => {
              return (
                <FormBlock
                  key={blockConfig.name + index}
                  blockConfig={blockConfig}
                  formik={formFormik}
                />
              );
          })
        }
        <FormBtn> Buscar </FormBtn>
      </div>  
    </form>
     );
}
 
export default Form;