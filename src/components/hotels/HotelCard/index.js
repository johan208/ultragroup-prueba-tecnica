import React from 'react'
import { ImPriceTag } from 'react-icons/im';
import { BsMoonStarsFill } from 'react-icons/bs';
const HotelCard = ({ hotel }) => {
    return ( 
        <article className='relative flex w-full sm:w-10/12 rounded bg-white my-4'>
            <img
                className='relative w-1/3 object-cover h-56 rounded-l-lg'
                src={ hotel.propertyImage.image.url }
                alt={ hotel.propertyImage.alt }
                
            />
            <main className='relative w-2/3 p-4 bg-white flex flex-col justify-between rounded-r-lg'>
                <div >
                    <h1 className='relative font-bold text-black'>{ hotel.name && hotel.name }</h1>
                    <h2 className='relative text-gray-500'> { hotel.neighborhood && hotel.neighborhood.name }</h2>
                </div>
                
                <div>
                    <div className='flex-none sm:flex justify-between mt-6'> 
                        <span className='relative text-green-600 mb-2'> Totalmente reembolsable</span>
                        <span className='relative flex items-center w-max bg-red-500 rounded text-white p-1'> <ImPriceTag className='mr-2'/> Precio secreto disponible </span>
                    </div>
                    <div className='flex justify-between mt-2'>
                        <div className='flex flex-col justify-between'>
                            <span className='relative flex items-center'> <BsMoonStarsFill className='mr-2' /> Acomula sellos</span>
                            <span className='relative text-gray-900 font-bold'>  { (hotel.reviews.score && hotel.reviews.total) && `${hotel.reviews.score}/10 (${hotel.reviews.total} opiniones)` } </span>
                        </div>
                        <div className='flex flex-col justify-between items-end'>
                            <span className='relative text-2xl text-black font-bold'> { (hotel.price && hotel.price.lead) && `${ hotel.price.lead.currencyInfo.code } ${ hotel.price.lead.formatted }` }</span>
                            <span className='relative text-gray-500 font-semibold'>  <small> { (hotel.price && hotel.price.strikeOut) && `Antes ${ hotel.price.strikeOut.currencyInfo.code } ${ hotel.price.strikeOut.formatted }` } </small> </span>
                        </div>
                    </div>
                </div>

            </main>
        </article>
     );
}
 
export default HotelCard;