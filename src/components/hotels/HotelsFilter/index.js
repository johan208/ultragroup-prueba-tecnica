import React, { useEffect, useContext, useState } from 'react'

import hotelsContext from '../../../context/hotels/hotelsContext'
//Components
import Form from '../../FormComponents/Form';

const HotelsFilter = ( { filter } ) => {
    const [filterDefault, setFilterDefault ] = useState(  null )
    const { loading, getAllHotelsAction } = useContext( hotelsContext ) 


    useEffect(() => {
        setFilterDefault(Object.keys(filter).length && filter)
    }, [ filter ])

    useEffect(() => {
        // console.log('filter', filter)
        if( filterDefault && !loading ) {
            // console.log('DESDE USEEFECT', filterDefault)
            getAllHotelsAction( filterDefault )
        }

      },[ filterDefault ])

    return ( 
        <div className='w-full flex justify-center'>
            <Form
                justify="justify-center"
            />
        </div>
     );
}
 
export default HotelsFilter;