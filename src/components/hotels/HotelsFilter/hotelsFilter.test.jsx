/** @jest-environment jsdom */
// import { render } from "@testing-library/react"
// import { getAllHotelsCall } from '../../../services/hotels.services'
// import HotelsState from '../../../context/hotels/hotelsState';
// import HotelsFilter from './index'
import axios from 'axios'

const clienteAxios = axios.create({
    baseURL: 'https://hotels4.p.rapidapi.com',
    headers: {
      'Access-Control-Allow-Origin' : '*',
      'content-type': 'application/json',
      'X-RapidAPI-Key' : "5d68095e62msh29e37eb37ede983p136a52jsna071226d4e3a",
      "X-RapidAPI-Host" : "hotels4.p.rapidapi.com"
    }
  });
  
  export const getAllHotelsCall = ( body ) => {
      return clienteAxios.post(`/properties/v2/list`, body);
    };

const getAllHotelsAction = async ( params ) => {

    try {
        // const p = params
        // const { checkIn, checkOut } = p
        // const checkInDateArray = checkIn.split('-')
        // const checkOutDateArray = checkOut.split('-')
        // console.log(checkInDateArray, "checkInDateArray")

        const payload = {...params}
        const { data } = await getAllHotelsCall( 
            payload
            // `destinationId=${params.destinationId}&pageNumber=${p.pageNumber}&pageSize=${p.pageSize}&checkIn=${p.checkIn}&checkOut=${p.checkOut}&adults1=${p.adults1}`
            
         )
        if( data.data ) {
            return data.data
        } else if ( data.errors ) {
            return data.errors
        }
    } catch(error) {
        return error.response && error.response.data.message ? error.response.data.message : error.message
    }
}

describe('Prueba api hoteles', () => {

    // test('Debe renderisar con los datos de los hoteles', () => {
    //     render(<HotelsState> <HotelsFilter /> </HotelsState>)

    // })
    test('Debe retornar datos de la api en forma de objeto', async () => {
        const params = {
            "destination": {
                "regionId": "6054439"
            },
            "checkInDate": {
                "day": 30,
                "month": 11,
                "year": 2022
            },
            "checkOutDate": {
                "day": 1,
                "month": 12,
                "year": 2022
            },
            "rooms": [
                {
                    "adults": 2
                }
            ],
            "resultsStartingIndex": 0,
            "resultsSize": 12
        }

        const data = await getAllHotelsAction( params )
        if( Object.keys(data).length ) {
            // console.log( data )
            expect( typeof data ).toBe( 'object' )
            
        } else {
            expect( typeof data ).toBe( 'string' )
        }
    })
})
