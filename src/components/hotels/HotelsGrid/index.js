import React, { useContext } from 'react'
//components
import HotelCard from '../HotelCard'
//context
import hotelsContext from '../../../context/hotels/hotelsContext'

const HotelsGrid = () => {

    const { loading, hotels } = useContext( hotelsContext ) 

    return ( 
        <div className="container flex flex-col mx-auto px-4 py-4">
            <span className='relative text-gray-600 font-semibold'> { hotels.summary ? hotels.summary.matchedPropertiesSize  + ' propiedades' : '# propiedades' }</span>
            <span className='relative text-gray-800 font-bold'> { ( hotels.filterMetadata && hotels.filterMetadata.neighborhoods.length) && hotels.filterMetadata.neighborhoods[0].name } </span>
            
            {
                (Object.keys( hotels ).length && !loading) && hotels.properties.map( (hotel, index) => (
                    <HotelCard key={ index } hotel={ hotel } />
                ))
            }
        </div>
     );
}
 
export default HotelsGrid;