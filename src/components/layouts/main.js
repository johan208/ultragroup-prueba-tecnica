import React from 'react'
import Header from '../commons/header'

const MainLayout = ({ children }) => {
    return ( 
        <main>
            <Header />
            <section className="relative w-full h-full py-40 min-h-screen">
                <div className="absolute top-0 w-full h-full bg-slate-100 bg-no-repeat bg-full"></div>
                {children}
            </section>
        </main>
     );
}
 
export default MainLayout;