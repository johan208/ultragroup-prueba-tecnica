import { useCallback } from 'react';
import Router from 'next/router'
import * as Yup from 'yup';

const inputs = [
  {
    name: 'destinationId',
    type: 'number',
    label: 'Destino',
    placeholder: 'Id destino',
    field: 'input',
  },
  {
    name: 'adults1',
    type: 'number',
    label: 'Adultos',
    placeholder: '# adultos',
    field: 'input',
  },
  {
    name: 'checkIn',
    type: 'date',
    label: 'Fecha de ingreso',
    placeholder: '',
    field: 'input',
  },
  {
    name: 'checkOut',
    type: 'date',
    label: 'Fecha de Salida',
    placeholder: '',
    field: 'input',
  }
]
const useFilterForm = ( ) => {

  const initialValues = {
    destinationId: '',
    checkIn: '',
    checkOut: '',
    adults1: '',
    pageNumber:1,
    pageSize: 15
  };

  const validationSchema = {
    destinationId: Yup.number().required('Campo obligatorio*'),
    checkIn: Yup.string().required('Campo obligatorio*'),
    checkOut: Yup.string().required('Campo obligatorio*'),
    adults1: Yup.number().required('Campo obligatorio*')
  };

  const handleSubmit = useCallback((values) => {
    console.log( values)
    Router.push({
      pathname: '/hotel-search',
      query: {
        destinationId: values.destinationId,
        checkIn: values.checkIn,
        checkOut: values.checkOut,
        adults1: values.adults1,
        pageNumber: values.pageNumber,
        pageSize: values.pageSize
      }
    })
    
  }, []);

  return {
    initialValues,
    validationSchema,
    handleSubmit,
    inputs: inputs,
    // handleTab
  };
};

export default useFilterForm;
