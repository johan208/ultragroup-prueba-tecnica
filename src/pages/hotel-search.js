import React from 'react'
import { useRouter } from 'next/router'
// Layout 
import Main from '../components/layouts/main'

//Components
import HotelsFilter from '../components/hotels/HotelsFilter'
import HotelsGrid from '../components/hotels/HotelsGrid'

const Hotels = () => {
  const router = useRouter()
  const params = router.query
  return (
    <>
      <HotelsFilter 
        filter={ params }
      />
      <HotelsGrid />
    </>
  )
}
Hotels.layout = Main

export default Hotels