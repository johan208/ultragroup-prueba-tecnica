import '../assets/styles/globals.css'
import Head from 'next/head';

//Hotel Context
import HotelsState from '../context/hotels/hotelsState';

function MyApp({ Component, pageProps }) {
  const Layout = Component.layout || (({ children }) => <>{children}</>);

  return (
    <HotelsState>
      <>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <title> Buscar Hoteles | UG</title>
        </Head>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </>
    </HotelsState>
  )
}


export default MyApp
