import React, { useReducer} from 'react';
import hotelsReducer from './hotelsReducer'
import hotelsContext from './hotelsContext';
import { GET_HOTELS_REQUEST, GET_HOTELS_SUCCESS, GET_HOTELS_FAIL } from './types';

//utils
import { showLoader, hideLoader } from '../../utils/loader';
//Services
import { getAllHotelsCall } from '../../services/hotels.services';
//adapters
import { createHotelsAdapter } from '../../adapters/hotels.adapters';

const hotelsState = props => {
        var initialState = {
            hotels: {},
            loading: false,
        };
    const [state, dispatch] = useReducer( hotelsReducer, initialState);
    

    const getAllHotelsAction = async ( params ) => {
        showLoader()
        dispatch({
            type:GET_HOTELS_REQUEST
        })
        try {
            const p = params
            const { checkIn, checkOut } = p
            const checkInDateArray = checkIn.split('-')
            const checkOutDateArray = checkOut.split('-')
            // console.log(checkInDateArray, "checkInDateArray")

            const payload = {
                "destination": {
                    "regionId": p.destinationId
                },
                "checkInDate": {
                    "day": parseInt(checkInDateArray[2]),
                    "month":  parseInt(checkInDateArray[1]),
                    "year":  parseInt(checkInDateArray[0])
                },
                "checkOutDate": {
                    "day":  parseInt(checkOutDateArray[2]),
                    "month":  parseInt(checkOutDateArray[1]),
                    "year":  parseInt(checkOutDateArray[0])
                },
                "rooms": [
                    {
                        "adults":  parseInt(p.adults1)
                    }
                ],
                "resultsStartingIndex": 0,
                "resultsSize":  parseInt(p.pageSize)
            }
            const { data } = await getAllHotelsCall( 
                payload
                // `destinationId=${params.destinationId}&pageNumber=${p.pageNumber}&pageSize=${p.pageSize}&checkIn=${p.checkIn}&checkOut=${p.checkOut}&adults1=${p.adults1}`
                
             )
            if( data.data ) {
                console.log(data.data)
                dispatch({
                    type:GET_HOTELS_SUCCESS,
                    payload: createHotelsAdapter( data.data )
                })
                hideLoader()
                return data.data
            } else if ( data.errors ) {
                dispatch({
                    type:GET_HOTELS_FAIL,
                    payload: error.response && error.response.data.message ? error.response.data.message : error.message
                })
                hideLoader()
                return data.errors
            }
        } catch(error) {
            dispatch({
                type:GET_HOTELS_FAIL,
                payload: error.response && error.response.data.message ? error.response.data.message : error.message
            })
            hideLoader()
            return error.response && error.response.data.message ? error.response.data.message : error.message
        }
    }

    return (
        <hotelsContext.Provider
            value={{
                hotels: state.hotels,
                loading:state.loading,
                getAllHotelsAction
            }}
        >
            {props.children}
        </hotelsContext.Provider>
    )
}

export default hotelsState;
