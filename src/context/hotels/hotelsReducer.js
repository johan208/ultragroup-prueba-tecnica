import { GET_HOTELS_REQUEST, GET_HOTELS_SUCCESS } from './types';

export default (state, action) => {
    switch(action.type) {

        case GET_HOTELS_REQUEST:
            return {
                ...state,
                loading: true
            }
        
        // case CREATE_QUOTATION_FAIL:
        //     return {
        //         ...state,
        //         loading:false,
        //         error: action.payload,
        //         userRelatedFridge: null
        //     }

        case GET_HOTELS_SUCCESS:
            return {
                ...state,
                loading:false,
                hotels: { ...action.payload }
            }

        default:
            return state;
    }
}
