/** @type {import('next').NextConfig} */

const nextConfig = {
  reactStrictMode: true,
  env: {
    rapidApiKey: "5d68095e62msh29e37eb37ede983p136a52jsna071226d4e3a",
    rapidApiHost: "hotels4.p.rapidapi.com"
  },
}

module.exports = nextConfig
