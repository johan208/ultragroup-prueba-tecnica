(() => {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 5125:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "f": () => (/* binding */ createHotelsAdapter)
/* harmony export */ });
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var createHotelsAdapter = hotels => _objectSpread({}, hotels.propertySearch);

/***/ }),

/***/ 941:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var hotelsContext = /*#__PURE__*/(0,react__WEBPACK_IMPORTED_MODULE_0__.createContext)();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (hotelsContext);

/***/ }),

/***/ 4142:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7293);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((state, action) => {
  switch (action.type) {
    case _types__WEBPACK_IMPORTED_MODULE_0__/* .GET_HOTELS_REQUEST */ .HG:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: true
      });
    // case CREATE_QUOTATION_FAIL:
    //     return {
    //         ...state,
    //         loading:false,
    //         error: action.payload,
    //         userRelatedFridge: null
    //     }

    case _types__WEBPACK_IMPORTED_MODULE_0__/* .GET_HOTELS_SUCCESS */ .Am:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: false,
        hotels: _objectSpread({}, action.payload)
      });

    default:
      return state;
  }
});

/***/ }),

/***/ 9795:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _hotelsReducer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4142);
/* harmony import */ var _hotelsContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(941);
/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(7293);
/* harmony import */ var _utils_loader__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5619);
/* harmony import */ var _services_hotels_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(235);
/* harmony import */ var _adapters_hotels_adapters__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(5125);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_services_hotels_services__WEBPACK_IMPORTED_MODULE_2__]);
_services_hotels_services__WEBPACK_IMPORTED_MODULE_2__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }




 //utils

 //Services

 //adapters




var hotelsState = props => {
  var initialState = {
    hotels: {},
    loading: false
  };
  var [state, dispatch] = (0,react__WEBPACK_IMPORTED_MODULE_0__.useReducer)(_hotelsReducer__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, initialState);

  var getAllHotelsAction = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(function* (params) {
      (0,_utils_loader__WEBPACK_IMPORTED_MODULE_5__/* .showLoader */ .V)();
      dispatch({
        type: _types__WEBPACK_IMPORTED_MODULE_6__/* .GET_HOTELS_REQUEST */ .HG
      });

      try {
        var p = params;
        var {
          checkIn,
          checkOut
        } = p;
        var checkInDateArray = checkIn.split('-');
        var checkOutDateArray = checkOut.split('-'); // console.log(checkInDateArray, "checkInDateArray")

        var payload = {
          "destination": {
            "regionId": p.destinationId
          },
          "checkInDate": {
            "day": parseInt(checkInDateArray[2]),
            "month": parseInt(checkInDateArray[1]),
            "year": parseInt(checkInDateArray[0])
          },
          "checkOutDate": {
            "day": parseInt(checkOutDateArray[2]),
            "month": parseInt(checkOutDateArray[1]),
            "year": parseInt(checkOutDateArray[0])
          },
          "rooms": [{
            "adults": parseInt(p.adults1)
          }],
          "resultsStartingIndex": 0,
          "resultsSize": parseInt(p.pageSize)
        };
        var {
          data
        } = yield (0,_services_hotels_services__WEBPACK_IMPORTED_MODULE_2__/* .getAllHotelsCall */ .k)(payload // `destinationId=${params.destinationId}&pageNumber=${p.pageNumber}&pageSize=${p.pageSize}&checkIn=${p.checkIn}&checkOut=${p.checkOut}&adults1=${p.adults1}`
        );

        if (data.data) {
          console.log(data.data);
          dispatch({
            type: _types__WEBPACK_IMPORTED_MODULE_6__/* .GET_HOTELS_SUCCESS */ .Am,
            payload: (0,_adapters_hotels_adapters__WEBPACK_IMPORTED_MODULE_7__/* .createHotelsAdapter */ .f)(data.data)
          });
          (0,_utils_loader__WEBPACK_IMPORTED_MODULE_5__/* .hideLoader */ .G)();
          return data.data;
        } else if (data.errors) {
          dispatch({
            type: _types__WEBPACK_IMPORTED_MODULE_6__/* .GET_HOTELS_FAIL */ .H5,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message
          });
          (0,_utils_loader__WEBPACK_IMPORTED_MODULE_5__/* .hideLoader */ .G)();
          return data.errors;
        }
      } catch (error) {
        dispatch({
          type: _types__WEBPACK_IMPORTED_MODULE_6__/* .GET_HOTELS_FAIL */ .H5,
          payload: error.response && error.response.data.message ? error.response.data.message : error.message
        });
        (0,_utils_loader__WEBPACK_IMPORTED_MODULE_5__/* .hideLoader */ .G)();
        return error.response && error.response.data.message ? error.response.data.message : error.message;
      }
    });

    return function getAllHotelsAction(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(_hotelsContext__WEBPACK_IMPORTED_MODULE_1__/* ["default"].Provider */ .Z.Provider, {
    value: {
      hotels: state.hotels,
      loading: state.loading,
      getAllHotelsAction
    },
    children: props.children
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (hotelsState);
__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 7293:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Am": () => (/* binding */ GET_HOTELS_SUCCESS),
/* harmony export */   "H5": () => (/* binding */ GET_HOTELS_FAIL),
/* harmony export */   "HG": () => (/* binding */ GET_HOTELS_REQUEST)
/* harmony export */ });
var GET_HOTELS_REQUEST = 'GET_HOTELS_REQUEST';
var GET_HOTELS_SUCCESS = 'GET_HOTELS_SUCCESS';
var GET_HOTELS_FAIL = 'GET_HOTELS_FAIL';

/***/ }),

/***/ 3669:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _assets_styles_globals_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8324);
/* harmony import */ var _assets_styles_globals_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_assets_styles_globals_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _context_hotels_hotelsState__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9795);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_hotels_hotelsState__WEBPACK_IMPORTED_MODULE_2__]);
_context_hotels_hotelsState__WEBPACK_IMPORTED_MODULE_2__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


 //Hotel Context






function MyApp(_ref) {
  var {
    Component,
    pageProps
  } = _ref;

  var Layout = Component.layout || (_ref2 => {
    var {
      children
    } = _ref2;
    return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
      children: children
    });
  });

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(_context_hotels_hotelsState__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_1___default()), {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx("meta", {
          name: "viewport",
          content: "width=device-width, initial-scale=1, shrink-to-fit=no"
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx("title", {
          children: " Buscar Hoteles | UG"
        })]
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(Layout, {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(Component, _objectSpread({}, pageProps))
      })]
    })
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyApp);
__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 235:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "k": () => (/* binding */ getAllHotelsCall)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9648);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([axios__WEBPACK_IMPORTED_MODULE_0__]);
axios__WEBPACK_IMPORTED_MODULE_0__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];

var clienteAxios = axios__WEBPACK_IMPORTED_MODULE_0__["default"].create({
  baseURL: 'https://hotels4.p.rapidapi.com',
  headers: {
    'Access-Control-Allow-Origin': '*',
    'content-type': 'application/json',
    'X-RapidAPI-Key': "5d68095e62msh29e37eb37ede983p136a52jsna071226d4e3a",
    "X-RapidAPI-Host": "hotels4.p.rapidapi.com"
  }
});
var getAllHotelsCall = body => {
  return clienteAxios.post("/properties/v2/list", body);
};
__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 5619:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "G": () => (/* binding */ hideLoader),
/* harmony export */   "V": () => (/* binding */ showLoader)
/* harmony export */ });
if (false) { var hideLoader, showLoader; }



/***/ }),

/***/ 8324:
/***/ (() => {



/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 9648:
/***/ ((module) => {

"use strict";
module.exports = import("axios");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(3669));
module.exports = __webpack_exports__;

})();