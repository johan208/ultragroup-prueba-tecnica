exports.id = 859;
exports.ids = [859];
exports.modules = {

/***/ 6324:
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";
var __webpack_unused_export__;


var _excluded = ["strategy", "src", "children", "dangerouslySetInnerHTML"],
    _excluded2 = ["strategy"],
    _excluded3 = ["crossOrigin", "nonce"],
    _excluded4 = ["strategy", "children", "dangerouslySetInnerHTML", "src"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

__webpack_unused_export__ = ({
  value: true
});
exports.Html = Html;
exports.Main = Main;
exports["default"] = void 0;

var _react = _interopRequireWildcard(__webpack_require__(6689));

var _constants = __webpack_require__(6724);

var _getPageFiles = __webpack_require__(4140);

var _htmlescape = __webpack_require__(9716);

var _isError = _interopRequireDefault(__webpack_require__(676));

var _htmlContext = __webpack_require__(8743);

class Document extends _react.default.Component {
  /**
  * `getInitialProps` hook returns the context object with the addition of `renderPage`.
  * `renderPage` callback executes `React` rendering logic synchronously to support server-rendering wrappers
  */
  static getInitialProps(ctx) {
    return ctx.defaultGetInitialProps(ctx);
  }

  render() {
    return /*#__PURE__*/_react.default.createElement(Html, null, /*#__PURE__*/_react.default.createElement(Head, null), /*#__PURE__*/_react.default.createElement("body", null, /*#__PURE__*/_react.default.createElement(Main, null), /*#__PURE__*/_react.default.createElement(NextScript, null)));
  }

}

exports["default"] = Document;

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || typeof obj !== "object" && typeof obj !== "function") {
    return {
      default: obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj.default = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

function getDocumentFiles(buildManifest, pathname, inAmpMode) {
  var sharedFiles = (0, _getPageFiles).getPageFiles(buildManifest, "/_app");
  var pageFiles =  true && inAmpMode ? [] : (0, _getPageFiles).getPageFiles(buildManifest, pathname);
  return {
    sharedFiles,
    pageFiles,
    allFiles: [...new Set([...sharedFiles, ...pageFiles])]
  };
}

function getPolyfillScripts(context, props) {
  // polyfills.js has to be rendered as nomodule without async
  // It also has to be the first script to load
  var {
    assetPrefix,
    buildManifest,
    devOnlyCacheBusterQueryString,
    disableOptimizedLoading,
    crossOrigin
  } = context;
  return buildManifest.polyfillFiles.filter(polyfill => polyfill.endsWith(".js") && !polyfill.endsWith(".module.js")).map(polyfill => /*#__PURE__*/_react.default.createElement("script", {
    key: polyfill,
    defer: !disableOptimizedLoading,
    nonce: props.nonce,
    crossOrigin: props.crossOrigin || crossOrigin,
    noModule: true,
    src: "".concat(assetPrefix, "/_next/").concat(polyfill).concat(devOnlyCacheBusterQueryString)
  }));
}

function hasComponentProps(child) {
  return !!child && !!child.props;
}

function AmpStyles(_ref) {
  var {
    styles
  } = _ref;
  if (!styles) return null; // try to parse styles from fragment for backwards compat

  var curStyles = Array.isArray(styles) ? styles : [];

  if ( // @ts-ignore Property 'props' does not exist on type ReactElement
  styles.props && // @ts-ignore Property 'props' does not exist on type ReactElement
  Array.isArray(styles.props.children)) {
    var hasStyles = el => {
      var ref, ref1;
      return el == null ? void 0 : (ref = el.props) == null ? void 0 : (ref1 = ref.dangerouslySetInnerHTML) == null ? void 0 : ref1.__html;
    }; // @ts-ignore Property 'props' does not exist on type ReactElement


    styles.props.children.forEach(child => {
      if (Array.isArray(child)) {
        child.forEach(el => hasStyles(el) && curStyles.push(el));
      } else if (hasStyles(child)) {
        curStyles.push(child);
      }
    });
  }
  /* Add custom styles before AMP styles to prevent accidental overrides */


  return /*#__PURE__*/_react.default.createElement("style", {
    "amp-custom": "",
    dangerouslySetInnerHTML: {
      __html: curStyles.map(style => style.props.dangerouslySetInnerHTML.__html).join("").replace(/\/\*# sourceMappingURL=.*\*\//g, "").replace(/\/\*@ sourceURL=.*?\*\//g, "")
    }
  });
}

function getDynamicChunks(context, props, files) {
  var {
    dynamicImports,
    assetPrefix,
    isDevelopment,
    devOnlyCacheBusterQueryString,
    disableOptimizedLoading,
    crossOrigin
  } = context;
  return dynamicImports.map(file => {
    if (!file.endsWith(".js") || files.allFiles.includes(file)) return null;
    return /*#__PURE__*/_react.default.createElement("script", {
      async: !isDevelopment && disableOptimizedLoading,
      defer: !disableOptimizedLoading,
      key: file,
      src: "".concat(assetPrefix, "/_next/").concat(encodeURI(file)).concat(devOnlyCacheBusterQueryString),
      nonce: props.nonce,
      crossOrigin: props.crossOrigin || crossOrigin
    });
  });
}

function getScripts(context, props, files) {
  var ref;
  var {
    assetPrefix,
    buildManifest,
    isDevelopment,
    devOnlyCacheBusterQueryString,
    disableOptimizedLoading,
    crossOrigin
  } = context;
  var normalScripts = files.allFiles.filter(file => file.endsWith(".js"));
  var lowPriorityScripts = (ref = buildManifest.lowPriorityFiles) == null ? void 0 : ref.filter(file => file.endsWith(".js"));
  return [...normalScripts, ...lowPriorityScripts].map(file => {
    return /*#__PURE__*/_react.default.createElement("script", {
      key: file,
      src: "".concat(assetPrefix, "/_next/").concat(encodeURI(file)).concat(devOnlyCacheBusterQueryString),
      nonce: props.nonce,
      async: !isDevelopment && disableOptimizedLoading,
      defer: !disableOptimizedLoading,
      crossOrigin: props.crossOrigin || crossOrigin
    });
  });
}

function getPreNextWorkerScripts(context, props) {
  var {
    assetPrefix,
    scriptLoader,
    crossOrigin,
    nextScriptWorkers
  } = context; // disable `nextScriptWorkers` in edge runtime

  if (!nextScriptWorkers || "nodejs" === "edge") return null;

  try {
    var {
      partytownSnippet
    } = require("@builder.io/partytown/integration");

    var children = Array.isArray(props.children) ? props.children : [props.children]; // Check to see if the user has defined their own Partytown configuration

    var userDefinedConfig = children.find(child => {
      var ref, ref2;
      return hasComponentProps(child) && (child == null ? void 0 : (ref = child.props) == null ? void 0 : (ref2 = ref.dangerouslySetInnerHTML) == null ? void 0 : ref2.__html.length) && "data-partytown-config" in child.props;
    });
    return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, !userDefinedConfig && /*#__PURE__*/_react.default.createElement("script", {
      "data-partytown-config": "",
      dangerouslySetInnerHTML: {
        __html: "\n            partytown = {\n              lib: \"".concat(assetPrefix, "/_next/static/~partytown/\"\n            };\n          ")
      }
    }), /*#__PURE__*/_react.default.createElement("script", {
      "data-partytown": "",
      dangerouslySetInnerHTML: {
        __html: partytownSnippet()
      }
    }), (scriptLoader.worker || []).map((file, index) => {
      var {
        strategy,
        src,
        children: scriptChildren,
        dangerouslySetInnerHTML
      } = file,
          scriptProps = _objectWithoutProperties(file, _excluded);

      var srcProps = {};

      if (src) {
        // Use external src if provided
        srcProps.src = src;
      } else if (dangerouslySetInnerHTML && dangerouslySetInnerHTML.__html) {
        // Embed inline script if provided with dangerouslySetInnerHTML
        srcProps.dangerouslySetInnerHTML = {
          __html: dangerouslySetInnerHTML.__html
        };
      } else if (scriptChildren) {
        // Embed inline script if provided with children
        srcProps.dangerouslySetInnerHTML = {
          __html: typeof scriptChildren === "string" ? scriptChildren : Array.isArray(scriptChildren) ? scriptChildren.join("") : ""
        };
      } else {
        throw new Error("Invalid usage of next/script. Did you forget to include a src attribute or an inline script? https://nextjs.org/docs/messages/invalid-script");
      }

      return /*#__PURE__*/_react.default.createElement("script", Object.assign({}, srcProps, scriptProps, {
        type: "text/partytown",
        key: src || index,
        nonce: props.nonce,
        "data-nscript": "worker",
        crossOrigin: props.crossOrigin || crossOrigin
      }));
    }));
  } catch (err) {
    if ((0, _isError).default(err) && err.code !== "MODULE_NOT_FOUND") {
      console.warn("Warning: ".concat(err.message));
    }

    return null;
  }
}

function getPreNextScripts(context, props) {
  var {
    scriptLoader,
    disableOptimizedLoading,
    crossOrigin
  } = context;
  var webWorkerScripts = getPreNextWorkerScripts(context, props);
  var beforeInteractiveScripts = (scriptLoader.beforeInteractive || []).filter(script => script.src).map((file, index) => {
    var _scriptProps$defer;

    var {
      strategy
    } = file,
        scriptProps = _objectWithoutProperties(file, _excluded2);

    return /*#__PURE__*/_react.default.createElement("script", Object.assign({}, scriptProps, {
      key: scriptProps.src || index,
      defer: (_scriptProps$defer = scriptProps.defer) !== null && _scriptProps$defer !== void 0 ? _scriptProps$defer : !disableOptimizedLoading,
      nonce: props.nonce,
      "data-nscript": "beforeInteractive",
      crossOrigin: props.crossOrigin || crossOrigin
    }));
  });
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, webWorkerScripts, beforeInteractiveScripts);
}

function getHeadHTMLProps(props) {
  var {
    crossOrigin,
    nonce
  } = props,
      restProps = _objectWithoutProperties(props, _excluded3); // This assignment is necessary for additional type checking to avoid unsupported attributes in <head>


  var headProps = restProps;
  return headProps;
}

function getAmpPath(ampPath, asPath) {
  return ampPath || "".concat(asPath).concat(asPath.includes("?") ? "&" : "?", "amp=1");
}

function getFontLoaderLinks(fontLoaderManifest, dangerousAsPath) {
  var assetPrefix = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";

  if (!fontLoaderManifest) {
    return {
      preconnect: null,
      preload: null
    };
  }

  var appFontsEntry = fontLoaderManifest.pages["/_app"];
  var pageFontsEntry = fontLoaderManifest.pages[dangerousAsPath];
  var preloadedFontFiles = [...(appFontsEntry !== null && appFontsEntry !== void 0 ? appFontsEntry : []), ...(pageFontsEntry !== null && pageFontsEntry !== void 0 ? pageFontsEntry : [])]; // If no font files should preload but there's an entry for the path, add a preconnect tag.

  var preconnectToSelf = !!(preloadedFontFiles.length === 0 && (appFontsEntry || pageFontsEntry));
  return {
    preconnect: preconnectToSelf ? /*#__PURE__*/_react.default.createElement("link", {
      rel: "preconnect",
      href: "/",
      crossOrigin: "anonymous"
    }) : null,
    preload: preloadedFontFiles ? preloadedFontFiles.map(fontFile => {
      var ext = /\.(woff|woff2|eot|ttf|otf)$/.exec(fontFile)[1];
      return /*#__PURE__*/_react.default.createElement("link", {
        key: fontFile,
        rel: "preload",
        href: "".concat(assetPrefix, "/_next/").concat(encodeURI(fontFile)),
        as: "font",
        type: "font/".concat(ext),
        crossOrigin: "anonymous"
      });
    }) : null
  };
}

class Head extends _react.default.Component {
  getCssLinks(files) {
    var {
      assetPrefix,
      devOnlyCacheBusterQueryString,
      dynamicImports,
      crossOrigin,
      optimizeCss,
      optimizeFonts
    } = this.context;
    var cssFiles = files.allFiles.filter(f => f.endsWith(".css"));
    var sharedFiles = new Set(files.sharedFiles); // Unmanaged files are CSS files that will be handled directly by the
    // webpack runtime (`mini-css-extract-plugin`).

    var unmangedFiles = new Set([]);
    var dynamicCssFiles = Array.from(new Set(dynamicImports.filter(file => file.endsWith(".css"))));

    if (dynamicCssFiles.length) {
      var existing = new Set(cssFiles);
      dynamicCssFiles = dynamicCssFiles.filter(f => !(existing.has(f) || sharedFiles.has(f)));
      unmangedFiles = new Set(dynamicCssFiles);
      cssFiles.push(...dynamicCssFiles);
    }

    var cssLinkElements = [];
    cssFiles.forEach(file => {
      var isSharedFile = sharedFiles.has(file);

      if (!optimizeCss) {
        cssLinkElements.push( /*#__PURE__*/_react.default.createElement("link", {
          key: "".concat(file, "-preload"),
          nonce: this.props.nonce,
          rel: "preload",
          href: "".concat(assetPrefix, "/_next/").concat(encodeURI(file)).concat(devOnlyCacheBusterQueryString),
          as: "style",
          crossOrigin: this.props.crossOrigin || crossOrigin
        }));
      }

      var isUnmanagedFile = unmangedFiles.has(file);
      cssLinkElements.push( /*#__PURE__*/_react.default.createElement("link", {
        key: file,
        nonce: this.props.nonce,
        rel: "stylesheet",
        href: "".concat(assetPrefix, "/_next/").concat(encodeURI(file)).concat(devOnlyCacheBusterQueryString),
        crossOrigin: this.props.crossOrigin || crossOrigin,
        "data-n-g": isUnmanagedFile ? undefined : isSharedFile ? "" : undefined,
        "data-n-p": isUnmanagedFile ? undefined : isSharedFile ? undefined : ""
      }));
    });

    if ( true && optimizeFonts) {
      cssLinkElements = this.makeStylesheetInert(cssLinkElements);
    }

    return cssLinkElements.length === 0 ? null : cssLinkElements;
  }

  getPreloadDynamicChunks() {
    var {
      dynamicImports,
      assetPrefix,
      devOnlyCacheBusterQueryString,
      crossOrigin
    } = this.context;
    return dynamicImports.map(file => {
      if (!file.endsWith(".js")) {
        return null;
      }

      return /*#__PURE__*/_react.default.createElement("link", {
        rel: "preload",
        key: file,
        href: "".concat(assetPrefix, "/_next/").concat(encodeURI(file)).concat(devOnlyCacheBusterQueryString),
        as: "script",
        nonce: this.props.nonce,
        crossOrigin: this.props.crossOrigin || crossOrigin
      });
    }) // Filter out nulled scripts
    .filter(Boolean);
  }

  getPreloadMainLinks(files) {
    var {
      assetPrefix,
      devOnlyCacheBusterQueryString,
      scriptLoader,
      crossOrigin
    } = this.context;
    var preloadFiles = files.allFiles.filter(file => {
      return file.endsWith(".js");
    });
    return [...(scriptLoader.beforeInteractive || []).map(file => /*#__PURE__*/_react.default.createElement("link", {
      key: file.src,
      nonce: this.props.nonce,
      rel: "preload",
      href: file.src,
      as: "script",
      crossOrigin: this.props.crossOrigin || crossOrigin
    })), ...preloadFiles.map(file => /*#__PURE__*/_react.default.createElement("link", {
      key: file,
      nonce: this.props.nonce,
      rel: "preload",
      href: "".concat(assetPrefix, "/_next/").concat(encodeURI(file)).concat(devOnlyCacheBusterQueryString),
      as: "script",
      crossOrigin: this.props.crossOrigin || crossOrigin
    }))];
  }

  getBeforeInteractiveInlineScripts() {
    var {
      scriptLoader
    } = this.context;
    var {
      nonce,
      crossOrigin
    } = this.props;
    return (scriptLoader.beforeInteractive || []).filter(script => !script.src && (script.dangerouslySetInnerHTML || script.children)).map((file, index) => {
      var {
        strategy,
        children,
        dangerouslySetInnerHTML,
        src
      } = file,
          scriptProps = _objectWithoutProperties(file, _excluded4);

      var html = "";

      if (dangerouslySetInnerHTML && dangerouslySetInnerHTML.__html) {
        html = dangerouslySetInnerHTML.__html;
      } else if (children) {
        html = typeof children === "string" ? children : Array.isArray(children) ? children.join("") : "";
      }

      return /*#__PURE__*/_react.default.createElement("script", Object.assign({}, scriptProps, {
        dangerouslySetInnerHTML: {
          __html: html
        },
        key: scriptProps.id || index,
        nonce: nonce,
        "data-nscript": "beforeInteractive",
        crossOrigin: crossOrigin || undefined
      }));
    });
  }

  getDynamicChunks(files) {
    return getDynamicChunks(this.context, this.props, files);
  }

  getPreNextScripts() {
    return getPreNextScripts(this.context, this.props);
  }

  getScripts(files) {
    return getScripts(this.context, this.props, files);
  }

  getPolyfillScripts() {
    return getPolyfillScripts(this.context, this.props);
  }

  makeStylesheetInert(node) {
    return _react.default.Children.map(node, c => {
      var ref5, ref3;

      if ((c == null ? void 0 : c.type) === "link" && (c == null ? void 0 : (ref5 = c.props) == null ? void 0 : ref5.href) && _constants.OPTIMIZED_FONT_PROVIDERS.some(_ref2 => {
        var {
          url
        } = _ref2;
        var ref, ref4;
        return c == null ? void 0 : (ref = c.props) == null ? void 0 : (ref4 = ref.href) == null ? void 0 : ref4.startsWith(url);
      })) {
        var newProps = _objectSpread(_objectSpread({}, c.props || {}), {}, {
          "data-href": c.props.href,
          href: undefined
        });

        return /*#__PURE__*/_react.default.cloneElement(c, newProps);
      } else if (c == null ? void 0 : (ref3 = c.props) == null ? void 0 : ref3.children) {
        var _newProps = _objectSpread(_objectSpread({}, c.props || {}), {}, {
          children: this.makeStylesheetInert(c.props.children)
        });

        return /*#__PURE__*/_react.default.cloneElement(c, _newProps);
      }

      return c;
    }).filter(Boolean);
  }

  render() {
    var _this$props$nonce, _this$props$nonce2;

    var {
      styles,
      ampPath,
      inAmpMode,
      hybridAmp,
      canonicalBase,
      __NEXT_DATA__,
      dangerousAsPath,
      headTags,
      unstable_runtimeJS,
      unstable_JsPreload,
      disableOptimizedLoading,
      optimizeCss,
      optimizeFonts,
      assetPrefix,
      fontLoaderManifest
    } = this.context;
    var disableRuntimeJS = unstable_runtimeJS === false;
    var disableJsPreload = unstable_JsPreload === false || !disableOptimizedLoading;
    this.context.docComponentsRendered.Head = true;
    var {
      head
    } = this.context;
    var cssPreloads = [];
    var otherHeadElements = [];

    if (head) {
      head.forEach(c => {
        if (c && c.type === "link" && c.props["rel"] === "preload" && c.props["as"] === "style") {
          cssPreloads.push(c);
        } else {
          c && otherHeadElements.push(c);
        }
      });
      head = cssPreloads.concat(otherHeadElements);
    }

    var children = _react.default.Children.toArray(this.props.children).filter(Boolean); // show a warning if Head contains <title> (only in development)


    if (false) {}

    if ( true && optimizeFonts && !( true && inAmpMode)) {
      children = this.makeStylesheetInert(children);
    }

    var hasAmphtmlRel = false;
    var hasCanonicalRel = false; // show warning and remove conflicting amp head tags

    head = _react.default.Children.map(head || [], child => {
      if (!child) return child;
      var {
        type,
        props
      } = child;

      if ( true && inAmpMode) {
        var badProp = "";

        if (type === "meta" && props.name === "viewport") {
          badProp = 'name="viewport"';
        } else if (type === "link" && props.rel === "canonical") {
          hasCanonicalRel = true;
        } else if (type === "script") {
          // only block if
          // 1. it has a src and isn't pointing to ampproject's CDN
          // 2. it is using dangerouslySetInnerHTML without a type or
          // a type of text/javascript
          if (props.src && props.src.indexOf("ampproject") < -1 || props.dangerouslySetInnerHTML && (!props.type || props.type === "text/javascript")) {
            badProp = "<script";
            Object.keys(props).forEach(prop => {
              badProp += " ".concat(prop, "=\"").concat(props[prop], "\"");
            });
            badProp += "/>";
          }
        }

        if (badProp) {
          console.warn("Found conflicting amp tag \"".concat(child.type, "\" with conflicting prop ").concat(badProp, " in ").concat(__NEXT_DATA__.page, ". https://nextjs.org/docs/messages/conflicting-amp-tag"));
          return null;
        }
      } else {
        // non-amp mode
        if (type === "link" && props.rel === "amphtml") {
          hasAmphtmlRel = true;
        }
      }

      return child;
    });
    var files = getDocumentFiles(this.context.buildManifest, this.context.__NEXT_DATA__.page,  true && inAmpMode);
    var fontLoaderLinks = getFontLoaderLinks(fontLoaderManifest, dangerousAsPath, assetPrefix);
    return /*#__PURE__*/_react.default.createElement("head", Object.assign({}, getHeadHTMLProps(this.props)), this.context.isDevelopment && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("style", {
      "data-next-hide-fouc": true,
      "data-ampdevmode":  true && inAmpMode ? "true" : undefined,
      dangerouslySetInnerHTML: {
        __html: "body{display:none}"
      }
    }), /*#__PURE__*/_react.default.createElement("noscript", {
      "data-next-hide-fouc": true,
      "data-ampdevmode":  true && inAmpMode ? "true" : undefined
    }, /*#__PURE__*/_react.default.createElement("style", {
      dangerouslySetInnerHTML: {
        __html: "body{display:block}"
      }
    }))), head, /*#__PURE__*/_react.default.createElement("meta", {
      name: "next-head-count",
      content: _react.default.Children.count(head || []).toString()
    }), children, optimizeFonts && /*#__PURE__*/_react.default.createElement("meta", {
      name: "next-font-preconnect"
    }), fontLoaderLinks.preconnect, fontLoaderLinks.preload,  true && inAmpMode && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("meta", {
      name: "viewport",
      content: "width=device-width,minimum-scale=1,initial-scale=1"
    }), !hasCanonicalRel && /*#__PURE__*/_react.default.createElement("link", {
      rel: "canonical",
      href: canonicalBase + (__webpack_require__(6368).cleanAmpPath)(dangerousAsPath)
    }), /*#__PURE__*/_react.default.createElement("link", {
      rel: "preload",
      as: "script",
      href: "https://cdn.ampproject.org/v0.js"
    }), /*#__PURE__*/_react.default.createElement(AmpStyles, {
      styles: styles
    }), /*#__PURE__*/_react.default.createElement("style", {
      "amp-boilerplate": "",
      dangerouslySetInnerHTML: {
        __html: "body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}"
      }
    }), /*#__PURE__*/_react.default.createElement("noscript", null, /*#__PURE__*/_react.default.createElement("style", {
      "amp-boilerplate": "",
      dangerouslySetInnerHTML: {
        __html: "body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}"
      }
    })), /*#__PURE__*/_react.default.createElement("script", {
      async: true,
      src: "https://cdn.ampproject.org/v0.js"
    })), !( true && inAmpMode) && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, !hasAmphtmlRel && hybridAmp && /*#__PURE__*/_react.default.createElement("link", {
      rel: "amphtml",
      href: canonicalBase + getAmpPath(ampPath, dangerousAsPath)
    }), this.getBeforeInteractiveInlineScripts(), !optimizeCss && this.getCssLinks(files), !optimizeCss && /*#__PURE__*/_react.default.createElement("noscript", {
      "data-n-css": (_this$props$nonce = this.props.nonce) !== null && _this$props$nonce !== void 0 ? _this$props$nonce : ""
    }), !disableRuntimeJS && !disableJsPreload && this.getPreloadDynamicChunks(), !disableRuntimeJS && !disableJsPreload && this.getPreloadMainLinks(files), !disableOptimizedLoading && !disableRuntimeJS && this.getPolyfillScripts(), !disableOptimizedLoading && !disableRuntimeJS && this.getPreNextScripts(), !disableOptimizedLoading && !disableRuntimeJS && this.getDynamicChunks(files), !disableOptimizedLoading && !disableRuntimeJS && this.getScripts(files), optimizeCss && this.getCssLinks(files), optimizeCss && /*#__PURE__*/_react.default.createElement("noscript", {
      "data-n-css": (_this$props$nonce2 = this.props.nonce) !== null && _this$props$nonce2 !== void 0 ? _this$props$nonce2 : ""
    }), this.context.isDevelopment && // this element is used to mount development styles so the
    // ordering matches production
    // (by default, style-loader injects at the bottom of <head />)

    /*#__PURE__*/
    _react.default.createElement("noscript", {
      id: "__next_css__DO_NOT_USE__"
    }), styles || null), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {}, ...(headTags || [])));
  }

}

_defineProperty(Head, "contextType", _htmlContext.HtmlContext);

exports.Head = Head;

function handleDocumentScriptLoaderItems(scriptLoader, __NEXT_DATA__, props) {
  var ref10, ref7, ref8, ref9;
  if (!props.children) return;
  var scriptLoaderItems = [];
  var children = Array.isArray(props.children) ? props.children : [props.children];
  var headChildren = (ref10 = children.find(child => child.type === Head)) == null ? void 0 : (ref7 = ref10.props) == null ? void 0 : ref7.children;
  var bodyChildren = (ref8 = children.find(child => child.type === "body")) == null ? void 0 : (ref9 = ref8.props) == null ? void 0 : ref9.children; // Scripts with beforeInteractive can be placed inside Head or <body> so children of both needs to be traversed

  var combinedChildren = [...(Array.isArray(headChildren) ? headChildren : [headChildren]), ...(Array.isArray(bodyChildren) ? bodyChildren : [bodyChildren])];

  _react.default.Children.forEach(combinedChildren, child => {
    var ref;
    if (!child) return; // When using the `next/script` component, register it in script loader.

    if ((ref = child.type) == null ? void 0 : ref.__nextScript) {
      if (child.props.strategy === "beforeInteractive") {
        scriptLoader.beforeInteractive = (scriptLoader.beforeInteractive || []).concat([_objectSpread({}, child.props)]);
        return;
      } else if (["lazyOnload", "afterInteractive", "worker"].includes(child.props.strategy)) {
        scriptLoaderItems.push(child.props);
        return;
      }
    }
  });

  __NEXT_DATA__.scriptLoader = scriptLoaderItems;
}

class NextScript extends _react.default.Component {
  getDynamicChunks(files) {
    return getDynamicChunks(this.context, this.props, files);
  }

  getPreNextScripts() {
    return getPreNextScripts(this.context, this.props);
  }

  getScripts(files) {
    return getScripts(this.context, this.props, files);
  }

  getPolyfillScripts() {
    return getPolyfillScripts(this.context, this.props);
  }

  static getInlineScriptSource(context) {
    var {
      __NEXT_DATA__,
      largePageDataBytes
    } = context;

    try {
      var data = JSON.stringify(__NEXT_DATA__);
      var bytes =  false ? 0 : Buffer.from(data).byteLength;

      var prettyBytes = (__webpack_require__(5955)/* ["default"] */ .Z);

      if (largePageDataBytes && bytes > largePageDataBytes) {
        console.warn("Warning: data for page \"".concat(__NEXT_DATA__.page, "\"").concat(__NEXT_DATA__.page === context.dangerousAsPath ? "" : " (path \"".concat(context.dangerousAsPath, "\")"), " is ").concat(prettyBytes(bytes), " which exceeds the threshold of ").concat(prettyBytes(largePageDataBytes), ", this amount of data can reduce performance.\nSee more info here: https://nextjs.org/docs/messages/large-page-data"));
      }

      return (0, _htmlescape).htmlEscapeJsonString(data);
    } catch (err) {
      if ((0, _isError).default(err) && err.message.indexOf("circular structure") !== -1) {
        throw new Error("Circular structure in \"getInitialProps\" result of page \"".concat(__NEXT_DATA__.page, "\". https://nextjs.org/docs/messages/circular-structure"));
      }

      throw err;
    }
  }

  render() {
    var {
      assetPrefix,
      inAmpMode,
      buildManifest,
      unstable_runtimeJS,
      docComponentsRendered,
      devOnlyCacheBusterQueryString,
      disableOptimizedLoading,
      crossOrigin
    } = this.context;
    var disableRuntimeJS = unstable_runtimeJS === false;
    docComponentsRendered.NextScript = true;

    if ( true && inAmpMode) {
      if (true) {
        return null;
      }

      var ampDevFiles = [...buildManifest.devFiles, ...buildManifest.polyfillFiles, ...buildManifest.ampDevFiles];
      return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, disableRuntimeJS ? null : /*#__PURE__*/_react.default.createElement("script", {
        id: "__NEXT_DATA__",
        type: "application/json",
        nonce: this.props.nonce,
        crossOrigin: this.props.crossOrigin || crossOrigin,
        dangerouslySetInnerHTML: {
          __html: NextScript.getInlineScriptSource(this.context)
        },
        "data-ampdevmode": true
      }), ampDevFiles.map(file => /*#__PURE__*/_react.default.createElement("script", {
        key: file,
        src: "".concat(assetPrefix, "/_next/").concat(file).concat(devOnlyCacheBusterQueryString),
        nonce: this.props.nonce,
        crossOrigin: this.props.crossOrigin || crossOrigin,
        "data-ampdevmode": true
      })));
    }

    if (false) {}

    var files = getDocumentFiles(this.context.buildManifest, this.context.__NEXT_DATA__.page,  true && inAmpMode);
    return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, !disableRuntimeJS && buildManifest.devFiles ? buildManifest.devFiles.map(file => /*#__PURE__*/_react.default.createElement("script", {
      key: file,
      src: "".concat(assetPrefix, "/_next/").concat(encodeURI(file)).concat(devOnlyCacheBusterQueryString),
      nonce: this.props.nonce,
      crossOrigin: this.props.crossOrigin || crossOrigin
    })) : null, disableRuntimeJS ? null : /*#__PURE__*/_react.default.createElement("script", {
      id: "__NEXT_DATA__",
      type: "application/json",
      nonce: this.props.nonce,
      crossOrigin: this.props.crossOrigin || crossOrigin,
      dangerouslySetInnerHTML: {
        __html: NextScript.getInlineScriptSource(this.context)
      }
    }), disableOptimizedLoading && !disableRuntimeJS && this.getPolyfillScripts(), disableOptimizedLoading && !disableRuntimeJS && this.getPreNextScripts(), disableOptimizedLoading && !disableRuntimeJS && this.getDynamicChunks(files), disableOptimizedLoading && !disableRuntimeJS && this.getScripts(files));
  }

}

_defineProperty(NextScript, "contextType", _htmlContext.HtmlContext);

exports.NextScript = NextScript;

function Html(props) {
  var {
    inAmpMode,
    docComponentsRendered,
    locale,
    scriptLoader,
    __NEXT_DATA__
  } = (0, _react).useContext(_htmlContext.HtmlContext);
  docComponentsRendered.Html = true;
  handleDocumentScriptLoaderItems(scriptLoader, __NEXT_DATA__, props);
  return /*#__PURE__*/_react.default.createElement("html", Object.assign({}, props, {
    lang: props.lang || locale || undefined,
    amp:  true && inAmpMode ? "" : undefined,
    "data-ampdevmode":  true && inAmpMode && false ? 0 : undefined
  }));
}

function Main() {
  var {
    docComponentsRendered
  } = (0, _react).useContext(_htmlContext.HtmlContext);
  docComponentsRendered.Main = true; // @ts-ignore

  return /*#__PURE__*/_react.default.createElement("next-js-internal-body-render-target", null);
} // Add a special property to the built-in `Document` component so later we can
// identify if a user customized `Document` is used or not.


var InternalFunctionDocument = function InternalFunctionDocument() {
  return /*#__PURE__*/_react.default.createElement(Html, null, /*#__PURE__*/_react.default.createElement(Head, null), /*#__PURE__*/_react.default.createElement("body", null, /*#__PURE__*/_react.default.createElement(Main, null), /*#__PURE__*/_react.default.createElement(NextScript, null)));
};

Document[_constants.NEXT_BUILTIN_DOCUMENT] = InternalFunctionDocument;

/***/ }),

/***/ 5955:
/***/ ((__unused_webpack_module, exports) => {

"use strict";
var __webpack_unused_export__;

__webpack_unused_export__ = ({
    value: true
});
exports.Z = prettyBytes;
function prettyBytes(number, options) {
    if (!Number.isFinite(number)) {
        throw new TypeError(`Expected a finite number, got ${typeof number}: ${number}`);
    }
    options = Object.assign({}, options);
    if (options.signed && number === 0) {
        return " 0 B";
    }
    const isNegative = number < 0;
    const prefix = isNegative ? "-" : options.signed ? "+" : "";
    if (isNegative) {
        number = -number;
    }
    if (number < 1) {
        const numberString = toLocaleString(number, options.locale);
        return prefix + numberString + " B";
    }
    const exponent = Math.min(Math.floor(Math.log10(number) / 3), UNITS.length - 1);
    number = Number((number / Math.pow(1000, exponent)).toPrecision(3));
    const numberString = toLocaleString(number, options.locale);
    const unit = UNITS[exponent];
    return prefix + numberString + " " + unit;
}
/*
MIT License

Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/ const UNITS = [
    "B",
    "kB",
    "MB",
    "GB",
    "TB",
    "PB",
    "EB",
    "ZB",
    "YB"
];
/*
Formats the given number using `Number#toLocaleString`.
- If locale is a string, the value is expected to be a locale-key (for example: `de`).
- If locale is true, the system default locale is used for translation.
- If no value for locale is specified, the number is returned unmodified.
*/ const toLocaleString = (number, locale)=>{
    let result = number;
    if (typeof locale === "string") {
        result = number.toLocaleString(locale);
    } else if (locale === true) {
        result = number.toLocaleString();
    }
    return result;
};

//# sourceMappingURL=pretty-bytes.js.map

/***/ }),

/***/ 6859:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__(6324)


/***/ })

};
;