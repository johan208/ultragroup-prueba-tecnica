"use strict";
exports.id = 627;
exports.ids = [627];
exports.modules = {

/***/ 6624:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ hotels_HotelsFilter)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./src/context/hotels/hotelsContext.js
var hotelsContext = __webpack_require__(941);
// EXTERNAL MODULE: external "formik"
var external_formik_ = __webpack_require__(2296);
// EXTERNAL MODULE: external "yup"
var external_yup_ = __webpack_require__(5609);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
var router_default = /*#__PURE__*/__webpack_require__.n(router_);
;// CONCATENATED MODULE: ./src/hooks/useFilterForm.js



var inputs = [{
  name: 'destinationId',
  type: 'number',
  label: 'Destino',
  placeholder: 'Id destino',
  field: 'input'
}, {
  name: 'adults1',
  type: 'number',
  label: 'Adultos',
  placeholder: '# adultos',
  field: 'input'
}, {
  name: 'checkIn',
  type: 'date',
  label: 'Fecha de ingreso',
  placeholder: '',
  field: 'input'
}, {
  name: 'checkOut',
  type: 'date',
  label: 'Fecha de Salida',
  placeholder: '',
  field: 'input'
}];

var useFilterForm = () => {
  var initialValues = {
    destinationId: '',
    checkIn: '',
    checkOut: '',
    adults1: '',
    pageNumber: 1,
    pageSize: 15
  };
  var validationSchema = {
    destinationId: external_yup_.number().required('Campo obligatorio*'),
    checkIn: external_yup_.string().required('Campo obligatorio*'),
    checkOut: external_yup_.string().required('Campo obligatorio*'),
    adults1: external_yup_.number().required('Campo obligatorio*')
  };
  var handleSubmit = (0,external_react_.useCallback)(values => {
    console.log(values);
    router_default().push({
      pathname: '/hotel-search',
      query: {
        destinationId: values.destinationId,
        checkIn: values.checkIn,
        checkOut: values.checkOut,
        adults1: values.adults1,
        pageNumber: values.pageNumber,
        pageSize: values.pageSize
      }
    });
  }, []);
  return {
    initialValues,
    validationSchema,
    handleSubmit,
    inputs: inputs // handleTab

  };
};

/* harmony default export */ const hooks_useFilterForm = (useFilterForm);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./src/components/FormComponents/ValidationError.js



var ValidationError = _ref => {
  var {
    formik,
    name
  } = _ref;

  // console.log('renderizado error =====>>>>>')
  if (formik.touched[name] && formik.errors[name]) {
    return /*#__PURE__*/jsx_runtime_.jsx("small", {
      className: "font-semibold leading-normal mt-2 mb-2 text-red-500",
      children: formik.errors[name]
    });
  }

  return null;
};

/* harmony default export */ const FormComponents_ValidationError = (/*#__PURE__*/(0,external_react_.memo)(ValidationError));
;// CONCATENATED MODULE: ./src/components/FormComponents/ControlContainer.js



var ControlContainer = _ref => {
  var {
    hidden,
    width,
    children
  } = _ref;
  var [anchura, setAnchura] = (0,external_react_.useState)('sm:w-6/12 lg:w-3/12 xl:w-3/12 w-6/12');
  (0,external_react_.useEffect)(() => {
    if (width === 'half') {
      setAnchura('sm:w-6/12 xl:w-4/12  w-6/12');
    }

    if (width === '2/3') {
      setAnchura('sm:w-6/12 xl:w-8/12  w-6/12');
    }

    if (width === 'full') {
      setAnchura('w-full');
    }
  }, [width]);
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: hidden ? 'hidden' : null + " justify-self-center items-center flex flex-col justify-start px-4 mb-4 ".concat(anchura),
    children: [' ', children, ' ']
  });
};

/* harmony default export */ const FormComponents_ControlContainer = (ControlContainer);
;// CONCATENATED MODULE: ./src/components/FormComponents/Input.js



var Input = _ref => {
  var {
    formik,
    type,
    name,
    placeholder
  } = _ref;
  return /*#__PURE__*/jsx_runtime_.jsx("input", {
    type: type,
    placeholder: placeholder ? placeholder : '',
    className: "px-3 py-3 placeholder-[#6B7280] text-[#6B7280] relative bg-white rounded text-base border text-sm shadow outline-none focus:shadow-md w-full",
    name: name,
    onChange: formik.handleChange,
    onBlur: formik.handleBlur,
    value: formik.values[name]
  });
};

/* harmony default export */ const FormComponents_Input = (Input);
;// CONCATENATED MODULE: ./src/components/FormComponents/Label.js



var Label = _ref => {
  var {
    children
  } = _ref;
  // console.log('renderizado label ====>>>>>>>>')
  return /*#__PURE__*/jsx_runtime_.jsx("label", {
    className: "relative self-start block uppercase text-black text-sm font-bold mb-2 ",
    children: children
  });
};

/* harmony default export */ const FormComponents_Label = (Label);
;// CONCATENATED MODULE: ./src/components/FormComponents/FormBlock.js
 // Components








var FormBlock = _ref => {
  var {
    blockConfig,
    formik
  } = _ref;

  if (blockConfig && blockConfig.field == 'input') {
    return /*#__PURE__*/(0,jsx_runtime_.jsxs)(FormComponents_ControlContainer, {
      hidden: blockConfig.hidden ? 'hidden' : null,
      width: blockConfig.fieldWidth && blockConfig.fieldWidth,
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(FormComponents_Label, {
        children: [" ", blockConfig.label, " "]
      }), /*#__PURE__*/jsx_runtime_.jsx(FormComponents_Input, {
        formik: formik,
        type: blockConfig.type === 'date' ? 'date' : blockConfig.type === 'number' ? 'number' : blockConfig.type === 'time' ? 'time' : 'text',
        placeholder: blockConfig.placeholder ? blockConfig.placeholder : null,
        name: blockConfig.name,
        isReadOnly: blockConfig.isReadOnly && blockConfig.isReadOnly
      }), /*#__PURE__*/jsx_runtime_.jsx(FormComponents_ValidationError, {
        formik: formik,
        name: blockConfig.name
      })]
    });
  }
};

/* harmony default export */ const FormComponents_FormBlock = (FormBlock);
;// CONCATENATED MODULE: ./src/components/FormComponents/FormBtn.js



var FormBtn = _ref => {
  var {
    type,
    children
  } = _ref;
  return /*#__PURE__*/jsx_runtime_.jsx("button", {
    className: 'relative bg-red-500 hover:bg-red-600  text-white active:bg-red-600 font-bold uppercase text-sm px-6 py-2 mt-1 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-4 mb-1 ease-linear transition-all duration-150',
    type: type ? type : 'submit',
    children: children
  });
};

/* harmony default export */ const FormComponents_FormBtn = (FormBtn);
;// CONCATENATED MODULE: ./src/components/FormComponents/Form.js



 // Components






var Form = _ref => {
  var {
    justify
  } = _ref;
  var {
    initialValues,
    validationSchema,
    handleSubmit,
    inputs
  } = hooks_useFilterForm();
  var formFormik = (0,external_formik_.useFormik)({
    enableReinitialize: true,
    initialValues: initialValues,
    validationSchema: external_yup_.object(validationSchema),
    onSubmit: handleSubmit
  });
  return /*#__PURE__*/jsx_runtime_.jsx("form", {
    onSubmit: formFormik.handleSubmit,
    className: 'bg-transparent shadow-lg relative rounded w-3/4 mb-12 p-6',
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: (justify ? justify : 'justify-between') + ' flex flex-wrap',
      children: [inputs.map((blockConfig, index) => {
        return /*#__PURE__*/jsx_runtime_.jsx(FormComponents_FormBlock, {
          blockConfig: blockConfig,
          formik: formFormik
        }, blockConfig.name + index);
      }), /*#__PURE__*/jsx_runtime_.jsx(FormComponents_FormBtn, {
        children: " Buscar "
      })]
    })
  });
};

/* harmony default export */ const FormComponents_Form = (Form);
;// CONCATENATED MODULE: ./src/components/hotels/HotelsFilter/index.js

 //Components




var HotelsFilter = _ref => {
  var {
    filter
  } = _ref;
  var [filterDefault, setFilterDefault] = (0,external_react_.useState)(null);
  var {
    loading,
    getAllHotelsAction
  } = (0,external_react_.useContext)(hotelsContext/* default */.Z);
  (0,external_react_.useEffect)(() => {
    setFilterDefault(Object.keys(filter).length && filter);
  }, [filter]);
  (0,external_react_.useEffect)(() => {
    // console.log('filter', filter)
    if (filterDefault && !loading) {
      // console.log('DESDE USEEFECT', filterDefault)
      getAllHotelsAction(filterDefault);
    }
  }, [filterDefault]);
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "w-full flex justify-center",
    children: /*#__PURE__*/jsx_runtime_.jsx(FormComponents_Form, {
      justify: "justify-center"
    })
  });
};

/* harmony default export */ const hotels_HotelsFilter = (HotelsFilter);

/***/ }),

/***/ 9390:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ main)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);
// EXTERNAL MODULE: external "react-icons/md"
var md_ = __webpack_require__(4041);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./src/components/commons/header/index.js

 //icons





var Header = () => {
  return /*#__PURE__*/jsx_runtime_.jsx("nav", {
    className: "top-0 fixed z-50 w-full px-2 py-3 navbar-expand-lg bg-white shadow",
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
      className: "flex flex-wrap items-center justify-end",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("li", {
        className: "flex items-center text-red-600 text-sm font-bold leading-relaxed mr-4 py-2",
        children: [/*#__PURE__*/jsx_runtime_.jsx(md_.MdLanguage, {
          className: "mr-1"
        }), /*#__PURE__*/jsx_runtime_.jsx((link_default()), {
          className: "",
          href: "#",
          children: "Espa\xF1ol"
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("li", {
        className: "text-red-600 text-sm font-bold leading-relaxed inline-block mr-4 py-2",
        children: /*#__PURE__*/jsx_runtime_.jsx((link_default()), {
          href: "#",
          children: "Iniciar Sesi\xF3n"
        })
      })]
    })
  });
};

/* harmony default export */ const header = (Header);
;// CONCATENATED MODULE: ./src/components/layouts/main.js





var MainLayout = _ref => {
  var {
    children
  } = _ref;
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("main", {
    children: [/*#__PURE__*/jsx_runtime_.jsx(header, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)("section", {
      className: "relative w-full h-full py-40 min-h-screen",
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "absolute top-0 w-full h-full bg-slate-100 bg-no-repeat bg-full"
      }), children]
    })]
  });
};

/* harmony default export */ const main = (MainLayout);

/***/ }),

/***/ 941:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var hotelsContext = /*#__PURE__*/(0,react__WEBPACK_IMPORTED_MODULE_0__.createContext)();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (hotelsContext);

/***/ })

};
;