# Dashboard Pintucotepinta

_Como líder de desarrollo deseo tener una landing que reciba parámetros por la URL del
navegador y retorna la lista de resultados de hoteles de una API para hacer pruebas de
búsqueda._

## Comenzando 🚀

_Se debe clonar el repositorio_

`$ git clone https://gitlab.com/johan208/pt-ultragroup.git`



### Pre-requisitos 📋

_NodeJS LTS_

```
https://nodejs.org/es/
```

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Instalar modulos_

```
npm install
#o
yarn install
```

_Ejecutar entorno de desarrollo_

```
npm run dev
#o
yarn dev
```

_Ejecutar entorno de producción_

```
npm run build
npm run start
#o
yarn build
yarn start
```

_webapp desplegada en la siguiente url:_
```https://grand-pie-bd0373.netlify.app/```

### Estructura

```sh
├── node_modules
├── public 
├── src
|   ├── adapters
|   ├── components
|   │   ├── commons
|   │   ├── formComponents
|   │   ├── hotels
|   │   └── layouts
|   ├── assets
|   |   └── styles
|   ├── hooks
|   ├── pages
|   |   ├── hotel-search
|   |   └── index
|   ├── services
|   ├── context
|   |   ├── hotels
|   |   |   ├── createContext
|   |   |   ├── actions
|   |   |   ├── reducers
|   |   |   └── types
|   └── utils
├── next.config.js
├── jest.config.js
├── babel.config.js
├── tailwind.config.js
└── package.json
```

## Construido con 🛠️


* [NextJs](https://nextjs.org/) - El framework web usado
* [Axios](axios) - Gestión de peticiones http 
* [Formik](https://formik.org/) - Gestión de formularios
* [Yup](https://docs.yup.io/) - Gestión del estado
* [TailwindCss](https://tailwindcss.com/) - Estilos


## Autores ✒️

* **Johan Garcia** - *Desarrollador web* - [JohanG](https://gitlab.com/johan208)
